% This script is used to fit a two-exponential decay model to sets of
% measured decay curves, by maximum likelihood estimation.
% This script calls the 'Convol' function, located in the 'Convol.m' file
% in the 'scripts' subfolder provided.
%
% INPUT: Experimental decay curves and measured IRF as CSV files (first
% column: times, second column: number of counts per pixel). The CSV files
% with experimental decay curves must be distributed in subfolders named
% according to the respective experimental conditions. Each subfolder must
% also contain one CSV file with the measured IRF that corresponds to the
% experimental settings used when measuring the FLIM-FRET decay curves of
% the subfolder.
%
% OUPUT: A MATLAB FIG file with boxplots of the fitted parameters
% ('TwoTimeScalesFit.fig'), and a TXT file with the fitted parameters for
% each provided decay curve ('Output_Fitting_2TS.txt').
%
% Note: it can be useful to set a breakpoint on the last instruction ("1")
% in order to save the generated variables, in particular the sets of
% fitted parameters.

function MLE_2exp()
path=''; %folder containing the CSV files, distributed into subfolders according to their respective experimental condition
list=dir(path);
DirecLabels=list([list.isdir]);
DirecLabels=DirecLabels(3:end);
close all

Pattern1='sensing'; %string pattern to be found in name of subfolder with force-sensing construct with or without drug data
Pattern2='ROCK';  %string pattern to be found in name of subfolder with force-sensing construct (or zero-force construct) with drug data
Pattern3='Zero'; %string pattern to be found in name of subfolder with zero-force construct data
Pattern4='only'; %string pattern to be found in name of subfolder with mCerulean only transfection data

DrugOnFS=1; %Set to 1 if the data in the 'ROCK' subfolders is from cells with force-sensing construct and drug present. Set to 0 if they are from cells with the zero-force construct and drug present.

%% parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ROI=[60,240]; % (channels) region of interest in the decay curve, for fitting. [60,240] excludes the emission peak and the end of the tail.
bgROI_IRF=[150,200]; % ROI for background substraction in the IRF curve.

% START VALUES
x0=[1.5; 3.4; 1/3; 0.1; 0];  % tau1 (ns), tau2 (ns), fraction of species 1, background contribution,(channel) IRF shift

% LOWER BOUNDS
lb= [0.2; 2; 0 ;0; 0]; % (ns) tau1, tau2, fraction of species 1, background contribution,(channel) IRF shift
% lb= [0.01; 0.01; 0 ;0; 0];%Alternative: free run

% UPPER BOUNDS
ub= [2; 4; 1; Inf; 0];
% ub= [10; 10; 1; Inf; 0]; %Alternative: free run







% CONSTRAINTS
% inequalities
Aineq=[];bineq=[];
Aineq=[Aineq;1,-1,0,0,0];bineq=[bineq;0]; % tau1<=tau2

% equalities
Aeq=[];beq=[];
% Aeq=[Aeq;1,0,0,0,0];beq=[beq;1] %Example: to set the value for tau1
% Aeq=[Aeq;0,1,0,0,0];beq=[beq;2.8] %Example: to set the value for tau2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% main script
addpath('scripts'); %to enable the use of the 'Convol' function

histD_total={[]};
tau1s=nan(4,30); tau2s=nan(4,30);
f1s=nan(4,30); 

% importing data
for DirecN=1:length(DirecLabels)
    Files=dir([path char(DirecLabels(DirecN).name) '\Cell*.csv']);
    for iterC=1:length(Files)
        
        csvFile=importdata([path DirecLabels(DirecN).name '\' Files(iterC).name]);
        histD_total{iterC}=csvFile.data(:,2);
        
        if iterC==1
            
            edgesT=csvFile.data(:,1)./1000; % Time bins (ns). This column must be the same accross all files of a given subfolder.
        end
    end
    
    % defining channels for fitting
    edges=(1:1:ROI(2));
    dtBin=(edgesT(2)-edgesT(1)); % (ns)
    xfine=(1:1:10000)';
    
    % loading IRF and background subtraction
    IRFs=dir([path char(DirecLabels(DirecN).name) '\IRF*.csv']);
    csvIRF=importdata([path DirecLabels(DirecN).name '\' IRFs(1).name]);
    IRF_G=csvIRF.data(:,2);
    IRF_G=IRF_G(1:ROI(2))-mean(IRF_G(bgROI_IRF(1):bgROI_IRF(2))); % background subtraction
    
    % normalization of IRF
    normIRF=IRF_G./trapz(edges,IRF_G);
    
    % fitting loop
    options=optimoptions(@fmincon,'Algorithm','sqp','Display','iter');    
    problem=createOptimProblem('fmincon','objective',@MLE_2Ir,'x0',x0,'Aeq',Aeq,'beq',beq,'Aineq',Aineq,'bineq',bineq,'lb',lb,'ub',ub,'options',options);
    problem.options.MaxIterations=1000;
    problem.options.MaxFunctionEvaluations=45000;
    
    arrParams=zeros(length(Files),length(x0));
    arr2I=zeros(length(Files),1);
    
    Average=0;

    for iterF=1:length(Files)
        
        Ni=histD_total{iterF}(ROI(1):ROI(2));
        NGG=sum(Ni);
        
        estMLE=fmincon(problem);
        
        arrParams(iterF,:)=estMLE;
        
        [opt_2Ir,opt_Mi]=MLE_2Ir(estMLE);
        
        arr2I(iterF)=opt_2Ir;
        
        figure(1); 
        subplot(3,1,[1 2]); hold off;
        p1=semilogy(edgesT(ROI(1):ROI(2)),Ni,'sk'); hold on
        set(p1,'Color',[0.5 0.5 0.5]);
        hold on;
        semilogy(edgesT(ROI(1):ROI(2)),opt_Mi,'-r','LineWidth',2);
        ylim([min(Ni)/2 2.*max(Ni)]);
        ylabel('counts');
        title([DirecLabels(DirecN).name '\' Files(iterF).name],'interpreter','none');
        legend('Data',['2I_r=' num2str(opt_2Ir)]);

        subplot(3,1,3); hold off;
        plot(edgesT(ROI(1):ROI(2)),(Ni-opt_Mi)./sqrt(Ni),'-k');hold on
        ylim([-1 1]);
        xlabel('time (ns)');
        ylabel('w. residual');
        Average=Average+Ni/max(Ni);
        
        
        % Save results in text file:[
        file_id = fopen([path 'Output_Fitting_2TS.txt'],'a');
        fprintf(file_id,'%s \t',Files(iterF).name);
        fprintf(file_id,'%g \t',estMLE,opt_2Ir);
        fprintf(file_id,'\n');
        fclose(file_id);
        
        if DrugOnFS %Case where the drug was added to cells expressing the force-sensing construct
            if (length(strfind(char(DirecLabels(DirecN).name),Pattern1))+length(strfind(char(DirecLabels(DirecN).name),Pattern2)))>1
                %condition 2: force-sensing with inhibitor
                index=min(find(isnan(tau1s(2,:))));
                tau1s(2,index)=estMLE(1);  tau2s(2,index)=estMLE(2); f1s(2,index)=estMLE(3);
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern1))==1
                 %condition 1: force-sensing without inhibitor
                index=min(find(isnan(tau1s(1,:))));
                tau1s(1,index)=estMLE(1);  tau2s(1,index)=estMLE(2); f1s(1,index)=estMLE(3);
            elseif (length(strfind(char(DirecLabels(DirecN).name),Pattern3))+length(strfind(char(DirecLabels(DirecN).name),Pattern2)))==1
                %condition 3: zero-force construct
                index=min(find(isnan(tau1s(3,:))));
                tau1s(3,index)=estMLE(1);  tau2s(3,index)=estMLE(2); f1s(3,index)=estMLE(3);
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern4))>0
                %condition 4: only cerulean
                index=min(find(isnan(tau1s(4,:))));
                tau1s(4,index)=estMLE(1);  tau2s(4,index)=estMLE(2); f1s(4,index)=estMLE(3);
            end
            
        else %Case where the drug was added to cells expressing the zero-force construc
            if (length(strfind(char(DirecLabels(DirecN).name),Pattern3))+length(strfind(char(DirecLabels(DirecN).name),Pattern2)))>1
                %condition 2prime: zero-force with inhibitor
                index=min(find(isnan(tau1s(2,:))));
                tau1s(2,index)=estMLE(1);  tau2s(2,index)=estMLE(2); f1s(2,index)=estMLE(3);
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern1))==1
                 %condition 1: force-sensing without inhibitor
                index=min(find(isnan(tau1s(1,:))));
                tau1s(1,index)=estMLE(1);  tau2s(1,index)=estMLE(2); f1s(1,index)=estMLE(3);
            elseif (length(strfind(char(DirecLabels(DirecN).name),Pattern3))+length(strfind(char(DirecLabels(DirecN).name),Pattern2)))==1
                %condition 3: zero-force construct
                index=min(find(isnan(tau1s(3,:))));
                tau1s(3,index)=estMLE(1);  tau2s(3,index)=estMLE(2); f1s(3,index)=estMLE(3);
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern4))>0
                %condition 4: only cerulean
                index=min(find(isnan(tau1s(4,:))));
                tau1s(4,index)=estMLE(1);  tau2s(4,index)=estMLE(2); f1s(4,index)=estMLE(3);
            end
        end
     end
    
    
    
end

figure(1); hold off
subplot(2,2,1);
boxplot(tau1s');
ylabel('\tau_1 (ns)');
[p12,h]=ranksum(tau1s(1,:),tau1s(2,:));
[p23,h]=ranksum(tau1s(2,:),tau1s(3,:));
title([num2str(p12) ' ',num2str(p23)]); %displays p-values from ranksum tests comparing the timescales from the 3 stFRET conditions
if DrugOnFS
xticklabels({'force-sensing','force-sens.+inhi.','zero-force','only Cerulean'})
else
xticklabels({'--','zero-force + inhib.','zero-force','--'})
end

subplot(2,2,2);
boxplot(tau2s');
ylabel('\tau_2 (ns)');
[p12,h]=ranksum(tau2s(1,:),tau2s(2,:));
[p23,h]=ranksum(tau2s(2,:),tau2s(3,:));
title([num2str(p12) ' ',num2str(p23)]);
if DrugOnFS
xticklabels({'force-sensing','force-sens.+inhi.','zero-force','only Cerulean'})
else
xticklabels({'--','zero-force + inhib.','zero-force','--'})
end

subplot(2,2,3);
boxplot(f1s');
ylabel('f1s');
[p12,h]=ranksum(f1s(1,:),f1s(2,:));
[p23,h]=ranksum(f1s(2,:),f1s(3,:));
title(['N=' num2str(sum(~isnan(f1s)')),'   ',num2str(p12) ' ',num2str(p23)]);
if DrugOnFS
xticklabels({'force-sensing','force-sens.+inhi.','zero-force','only Cerulean'})
else
xticklabels({'--','zero-force + inhib.','zero-force','--'})
end

saveas(gcf,[path, 'TwoTimeScalesFit' num2str(ROI(1)) ' ' num2str(ROI(2)) '.fig'])


1 % Set a breakpoint here to save the workspace 

    function [gof,Mi,shiftIRF]=MLE_2Ir(params)
        
        Tau1=params(1);
        Tau2=params(2);
        frac1=params(3);
        c=params(4);
        shift=params(5);
        
        if shift>=0
            shiftIRF=[zeros(round(shift),1);normIRF(1:end-round(shift))];
        else
            shiftIRF=[normIRF(1+abs(round(shift)):end);zeros(abs(round(shift)),1)];
        end
        
        f=frac1.*exp(-xfine.*dtBin./Tau1)./Tau1+(1-frac1).*exp(-xfine.*dtBin./Tau2)./Tau2;
        
        convfBG=Convol(shiftIRF,f)+c;
        
        fGG=convfBG(ROI(1):ROI(2))./sum(convfBG(ROI(1):ROI(2)));
        
        Mi=NGG.*fGG;
        
        gof=2/(length(Ni)-length(params))*sum(Ni.*log(Ni./Mi)); % MLE
        %       gof=sum(((Ni-Mi).^2)./Ni)/(length(Ni)-length(params)-1); % red. Chi2

    end
end