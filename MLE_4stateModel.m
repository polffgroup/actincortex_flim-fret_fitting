% This script is used to fit our four-state FLIM-FRET decay model to
% sets of measured decay curves, by maximum likelihood estimation.
% This script calls the 'Convol' function, located in the 'Convol.m' file
% in the 'scripts' subfolder provided.
%
% The script first runs a two-exponential fit (like 'MLE_2exp.m') on all
% datasets. And then it fits the four-state decay model to the
% datasets of the conditions with stFRET constructs. In this second step,
% parameters 'kr1', 'kr2' and 'pr1' are inferred from results of the first
% step, and parameters 'ke' (here its reciprocal) and 'pe' are
% fitted.
%
% INPUT: Experimental decay curves and measured IRF as CSV files (first
% column: times, second column: number of counts per pixel). The CSV files
% with experimental decay curves must be distributed in subfolders named
% according to the respective experimental conditions. Each subfolder must
% also contain one CSV file with the measured IRF that corresponds to the
% experimental settings used when measuring the FLIM-FRET decay curves of
% the subfolder.
%
% OUPUT: Two MATLAB FIG files; one with boxplots of the parameters of an
% initial two-exponential fit (datasets from all conditions,
% 'TwoTimeScalesFit.fig'); and the other one with boxplots of the
% parameters specific to the four-state model (datasets from the
% conditions with stFRET constructs only, '4stateModelFit.fig').
%
% Note: it can be useful to set a breakpoint on the last instruction ("1")
% in order to save the generated variables, in particular the sets of
% fitted parameters and the chi-square values 'Chi2a' and 'Chi2b'.



function MLE_4stateModel()
path=''; % folder containing the subfolders with CSV files
list=dir(path);
DirecLabels=list([list.isdir]);
DirecLabels=DirecLabels(3:end);
close all

Pattern1='sensing'; %string pattern to be found in name of subfolder with force-sensing construct with or without drug data
Pattern2='ROCK';  %string pattern to be found in name of subfolder with force-sensing construct (or zero-force construct) with drug data
Pattern3='Zero'; %string pattern to be found in name of subfolder with zero-force construct data
Pattern4='only'; %string pattern to be found in name of subfolder with mCerulean only transfection data

DrugOnFS=1; %Set to 1 if the data in the 'ROCK' subfolders is from cells with force-sensing construct and drug present. Set to 0 if they are from cells with the zero-force construct and drug present.

%% parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Chi2a=[]; Chi2b=[];


ROI=[60,240]; % (channels) region of interest in the decay curve, for fitting. [60,240] excludes the emission peak and the end of the tail.
bgROI_IRF=[150,200]; % ROI for background substraction in the IRF curve.

% START VALUES
x0=[0.5; 3.2; 1/3; 0.1; 0];  % tau1 (ns), tau2 (ns), fraction of species 1, background contribution,(channel) IRF shift
x0b=[2; 0.2; 0.1; 0];% 1/ke, pe, background contribution,(channel) IRF shift

% LOWER BOUNDS
lb= [0.01; 0; 0 ;0; 0];% free run
lbb=[0.01; 0;0; 0];% free run
%lbb=[2.3; 0;0; 0];% To have a common ke accross conditions

% UPPER BOUNDS
ub= [10; 10; 1; Inf; 0]; %free run
ubb=[10; 1; Inf; 0];%free run
%ubb=[2.301; 1; Inf; 0];%To have a common ke accross conditions

% CONSTRAINTS
% inequalities
Aineq=[];bineq=[];
Aineq=[Aineq;1,-1,0,0,0];bineq=[bineq;0]; % tau1<=tau2

% equalities
Aeq=[];beq=[];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% main script
addpath('scripts'); %to enable the use of the 'Convol' function

histD_total={[]};
tau1s=nan(4,30); tau2s=nan(4,30);
f1s=nan(4,30);

%First fit: two-exponential decay model. On every dataset.
        
% importing data
for DirecN=1:length(DirecLabels)
    Files=dir([path char(DirecLabels(DirecN).name) '\Cell*.csv']);
    for iterC=1:length(Files)

        csvFile=importdata([path DirecLabels(DirecN).name '\' Files(iterC).name]);
        histD_total{iterC}=csvFile.data(:,2);

        if iterC==1

            edgesT=csvFile.data(:,1)./1000; % Time bins (ns). This column must be the same accross all files of a given subfolder.
        end
    end

    % defining channels for fitting
    edges=(1:1:ROI(2));
    dtBin=(edgesT(2)-edgesT(1)); % (ns)
    xfine=(1:1:10000)';

    % loading IRF and background subtraction
    IRFs=dir([path char(DirecLabels(DirecN).name) '/IRF*.csv']); % Each subfolder with FLIM measurements must contain a file with the time series of the IRF corresponding to the FLIM measurements.
    csvIRF=importdata([path DirecLabels(DirecN).name '\' IRFs(1).name]);
    IRF_G=csvIRF.data(:,2);
    IRF_G=IRF_G(1:ROI(2))-mean(IRF_G(bgROI_IRF(1):bgROI_IRF(2))); % background subtraction

    % normalization of IRF
    normIRF=IRF_G./trapz(edges,IRF_G);

    % fitting loop
    options=optimoptions(@fmincon,'Algorithm','sqp','Display','iter');
    problem=createOptimProblem('fmincon','objective',@MLE_2Ir,'x0',x0,'Aeq',Aeq,'beq',beq,'Aineq',Aineq,'bineq',bineq,'lb',lb,'ub',ub,'options',options);
    problem.options.MaxIterations=1000;
    problem.options.MaxFunctionEvaluations=45000;

    arrParams=zeros(length(Files),length(x0));
    arr2Ia=zeros(length(Files),1);
    Average=0;

    for iterF=1:length(Files)

        Ni=histD_total{iterF}(ROI(1):ROI(2));
        NGG=sum(Ni);

        estMLE=fmincon(problem);

        arrParams(iterF,:)=estMLE;

        [opt_2Ir,opt_Mi]=MLE_2Ir(estMLE);
        Chi2a=[Chi2a sum(((Ni-opt_Mi).^2)./Ni)/(length(Ni)-length(x0)-1)];

        arr2Ia(iterF)=opt_2Ir;

        figure(1);
        subplot(3,1,[1 2]); hold off;
        p1=semilogy(edgesT(ROI(1):ROI(2)),Ni,'sk'); hold on
        set(p1,'Color',[0.5 0.5 0.5]);
        hold on;
        semilogy(edgesT(ROI(1):ROI(2)),opt_Mi,'-r','LineWidth',2);
        ylim([min(Ni)/2 2.*max(Ni)]);
        ylabel('counts');
        title(Files(iterF).name,'interpreter','none');
        legend('Data',['2I_r=' num2str(opt_2Ir)]);

        subplot(3,1,3); hold off;
        plot(edgesT(ROI(1):ROI(2)),(Ni-opt_Mi)./sqrt(Ni),'-k');hold on
        ylim([-0.1 0.1]);
        xlabel('time (ns)');
        ylabel('w. residual');
        Average=Average+Ni/max(Ni);

        if DrugOnFS %Case where the drug was added to cells expressing the force-sensing construct
            if (length(strfind(char(DirecLabels(DirecN).name),Pattern1))+length(strfind(char(DirecLabels(DirecN).name),Pattern2)))>1
                %condition 2: force-sensing with inhibitor
                index=min(find(isnan(tau1s(2,:))));
                tau1s(2,index)=estMLE(1);  tau2s(2,index)=estMLE(2); f1s(2,index)=estMLE(3);
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern1))==1
                %condition 1: force-sensing without inhibitor
                index=min(find(isnan(tau1s(1,:))));
                tau1s(1,index)=estMLE(1); tau2s(1,index)=estMLE(2); f1s(1,index)=estMLE(3);
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern3))>0
                %condition 3: zero-force construct
                index=min(find(isnan(tau1s(3,:))));
                tau1s(3,index)=estMLE(1);  tau2s(3,index)=estMLE(2); f1s(3,index)=estMLE(3);
            else
                %condition 4: only cerulean
                index=min(find(isnan(tau1s(4,:))));
                tau1s(4,index)=estMLE(1);  tau2s(4,index)=estMLE(2); f1s(4,index)=estMLE(3);
            end

        else %Case where the drug was added to cells expressing the zero-force construct    
            if (length(strfind(char(DirecLabels(DirecN).name),Pattern3))+length(strfind(char(DirecLabels(DirecN).name),Pattern2)))>1
                %condition 2prime: zero-force with inhibitor
                index=min(find(isnan(tau1s(2,:))));
                tau1s(2,index)=estMLE(1);  tau2s(2,index)=estMLE(2); f1s(2,index)=estMLE(3);
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern1))==1
                %condition 1: force-sensing without inhibitor
                index=min(find(isnan(tau1s(1,:))));
                tau1s(1,index)=estMLE(1); tau2s(1,index)=estMLE(2); f1s(1,index)=estMLE(3);
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern3))>0
                %condition 3: zero-force construct
                index=min(find(isnan(tau1s(3,:))));
                tau1s(3,index)=estMLE(1);  tau2s(3,index)=estMLE(2); f1s(3,index)=estMLE(3);
            else
                %condition 4: only cerulean
                index=min(find(isnan(tau1s(4,:))));
                tau1s(4,index)=estMLE(1);  tau2s(4,index)=estMLE(2); f1s(4,index)=estMLE(3);
            end
        end
    end
end



figure(2); hold off
subplot(2,2,1);
boxplot(tau1s');
ylabel('\tau_1 (ns)');
[p12,h]=ranksum(tau1s(1,:),tau1s(2,:));
[p23,h]=ranksum(tau1s(2,:),tau1s(3,:));
title([num2str(p12) ' ',num2str(p23)]); %displays p-values from ranksum tests comparing the timescales from the 3 stFRET conditions
if DrugOnFS
xticklabels({'force-sensing','force-sens.+inhi.','zero-force','only Cerulean'})
else
xticklabels({'--','zero-force + inhib.','zero-force','--'})
end

subplot(2,2,2);
boxplot(tau2s');
ylabel('\tau_2 (ns)');
[p12,h]=ranksum(tau2s(1,:),tau2s(2,:));
[p23,h]=ranksum(tau2s(2,:),tau2s(3,:));
title([num2str(p12) ' ',num2str(p23)]);
if DrugOnFS
xticklabels({'force-sensing','force-sens.+inhi.','zero-force','only Cerulean'})
else
xticklabels({'--','zero-force + inhib.','zero-force','--'})
end

subplot(2,2,3);
boxplot(f1s');
ylabel('f1s');
[p12,h]=ranksum(f1s(1,:),f1s(2,:));
[p23,h]=ranksum(f1s(2,:),f1s(3,:));
title(['N=' num2str(sum(~isnan(f1s)')),'   ',num2str(p12) ' ',num2str(p23)]);
if DrugOnFS
xticklabels({'force-sensing','force-sens.+inhi.','zero-force','only Cerulean'})
else
xticklabels({'--','zero-force + inhib.','zero-force','--'})
end

saveas(gcf,[path, 'TwoTimeScalesFit' num2str(ROI(1)) ' ' num2str(ROI(2)) '.fig'])


% Second fit: model with two donor confomations and FRET (four-state model). Only on the datasets of conditions with stFRET constructs.

% Three of the new parameters are taken equal to the median values of the
% parameters from the two-exponential fit in the donor-only condition:
kr1=median(1./tau1s(4,:),'omitnan'); % radiative decay rate for donor in conformation 1
kr2=median(1./tau2s(4,:),'omitnan'); % radiative decay rate in conformation 2
pr1=median(f1s(4,:),'omitnan'); % fraction of mature donors in conformation 1

%The two remaining parameters will be fitted:
tau1sB=nan(4,30); % reciprocal of k_e,  the rate of decay through FRET
pes=nan(4,30); % probability for a mature donor to be in a FRET-enabled

% CONSTRAINTS
% inequalities
Aineq=[];bineq=[];

for DirecN=1:length(DirecLabels)
    if length(strfind(char(DirecLabels(DirecN).name),Pattern4))==0 % excludes the donor-only measurements
        Files=dir([path char(DirecLabels(DirecN).name) '\Cell*.csv']);
        for iterC=1:length(Files)

            csvFile=importdata([path DirecLabels(DirecN).name '\' Files(iterC).name]);
            histD_total{iterC}=csvFile.data(:,2);

            if iterC==1

                edgesT=csvFile.data(:,1)./1000; % (ns)
            end
        end

        % defining channels for fitting
        edges=(1:1:ROI(2));
        dtBin=(edgesT(2)-edgesT(1)); % (ns)
        xfine=(1:1:10000)';

        % loading IRF and background substraction
        IRFs=dir([path char(DirecLabels(DirecN).name) '\IRF*.csv']);
        csvIRF=importdata([path DirecLabels(DirecN).name '\' IRFs(1).name]);
        IRF_G=csvIRF.data(:,2);
        IRF_G=IRF_G(1:ROI(2))-mean(IRF_G(bgROI_IRF(1):bgROI_IRF(2)));

        % normalization of IRF
        normIRF=IRF_G./trapz(edges,IRF_G);

        % fitting loop
        options=optimoptions(@fmincon,'Algorithm','sqp','Display','iter');
        problem=createOptimProblem('fmincon','objective',@MLE_New,'x0',x0b,'lb',lbb,'ub',ubb,'options',options);
        problem.options.MaxIterations=1000;
        problem.options.MaxFunctionEvaluations=45000;

        arrParams=zeros(length(Files),length(x0b));
        arr2Ib=zeros(length(Files),1);

        Average=0;

        for iterF=1:length(Files)

            Ni=histD_total{iterF}(ROI(1):ROI(2));
            NGG=sum(Ni);

            estMLE=fmincon(problem);

            arrParams(iterF,:)=estMLE;

            [opt_2Ir,opt_Mi]=MLE_New(estMLE);
            Chi2b=[Chi2b sum(((Ni-opt_Mi).^2)./Ni)/(length(Ni)-length(x0)-1)];

            arr2Ib(iterF)=opt_2Ir;

            figure(1);
            subplot(3,1,[1 2]); hold off;
            p1=semilogy(edgesT(ROI(1):ROI(2)),Ni,'sk'); hold on
            set(p1,'Color',[0.5 0.5 0.5]);
            hold on;
            semilogy(edgesT(ROI(1):ROI(2)),opt_Mi,'-r','LineWidth',2);
            ylim([min(Ni)/2 2.*max(Ni)]);
            ylabel('counts');
            title([DirecLabels(DirecN).name '\' Files(iterF).name],'interpreter','none');
            legend('Data',['2I_r=' num2str(opt_2Ir)]);

            subplot(3,1,3); hold off;
            plot(edgesT(ROI(1):ROI(2)),(Ni-opt_Mi)./sqrt(Ni),'-k');hold on
            ylim([-0.1 0.1]);
            xlabel('time (ns)');
            ylabel('w. residual');


            if DrugOnFS %Case where the drug was added to cells expressing the force-sensing construct
                if (length(strfind(char(DirecLabels(DirecN).name),Pattern1))+length(strfind(char(DirecLabels(DirecN).name),Pattern2)))>1
                    %condition 2: force-sensing with inhibitor
                    index=min(find(isnan(tau1sB(2,:))));
                    tau1sB(2,index)=estMLE(1);  pes(2,index)=estMLE(2);
                elseif length(strfind(char(DirecLabels(DirecN).name),Pattern1))==1
                    %condition 1: force-sensing without inhibitor
                    index=min(find(isnan(tau1sB(1,:))));
                    tau1sB(1,index)=estMLE(1); pes(1,index)=estMLE(2);
                elseif length(strfind(char(DirecLabels(DirecN).name),Pattern3))>0
                    %condition 3: zero-force construct
                    index=min(find(isnan(tau1sB(3,:))));
                    tau1sB(3,index)=estMLE(1);  pes(3,index)=estMLE(2);
                end
            else %Case where the drug was added to cells expressing the zero-force construct
                if (length(strfind(char(DirecLabels(DirecN).name),Pattern3))+length(strfind(char(DirecLabels(DirecN).name),Pattern2)))>1
                    %condition 2prime: zero-force with inhibitor
                    index=min(find(isnan(tau1sB(2,:))));
                    tau1sB(2,index)=estMLE(1);  pes(2,index)=estMLE(2);
                elseif length(strfind(char(DirecLabels(DirecN).name),Pattern1))==1
                    %condition 1: force-sensing without inhibitor
                    index=min(find(isnan(tau1sB(1,:))));
                    tau1sB(1,index)=estMLE(1); pes(1,index)=estMLE(2);
                elseif length(strfind(char(DirecLabels(DirecN).name),Pattern3))>0
                    %condition 3: zero-force construct
                    index=min(find(isnan(tau1sB(3,:))));
                    tau1sB(3,index)=estMLE(1);  pes(3,index)=estMLE(2);
                end
            end
        end
    end
end



figure(3); hold off
subplot(2,1,1);
boxplot([tau1sB(1:3,:); tau1s(4,:); tau2s(4,:)]');
ylabel('1/k_e (ns)');
[p12,h]=ranksum(tau1sB(1,:),tau1sB(2,:));
[p23,h]=ranksum(tau1sB(2,:),tau1sB(3,:));
title(['N=' num2str(sum(~isnan(pes)')) ,'   ',num2str(p12) ' ',num2str(p23)]);
xticklabels({'force-sensing, 1/k_e','force-sens.+inhi., 1/k_e','zero-force, 1/k_e','only Cerulean, 1/k_r1', 'only Cerulean, 1/kr2'})


subplot(2,1,2);
boxplot([pes(1:3,:); f1s(4,:)]');
ylabel('p_e');
[p12,h]=ranksum(pes(1,:),pes(2,:));
[p23,h]=ranksum(pes(2,:),pes(3,:));
title(['N=' num2str(sum(~isnan(pes)')) ,'   ',num2str(p12) ' ',num2str(p23)]);
xticklabels({'force-sensing, p_e(1-f_scl)','force-sens. w. inh., p_e','zero-force, p_e','only Cerulean, p_r1'})
saveas(gcf,[path, '4stateModelFit' num2str(ROI(1)) ' ' num2str(ROI(2)) '.fig'])


1 % Set a breakpoint here to save the workspace 

    function [gof,Mi,shiftIRF]=MLE_2Ir(params)
        
        Tau1=params(1);
        Tau2=params(2);
        frac1=params(3);
        c=params(4);
        shift=params(5);
        
        if shift>=0
            shiftIRF=[zeros(round(shift),1);normIRF(1:end-round(shift))];
        else
            shiftIRF=[normIRF(1+abs(round(shift)):end);zeros(abs(round(shift)),1)];
        end
        
        f=frac1.*exp(-xfine.*dtBin./Tau1)./Tau1+(1-frac1).*exp(-xfine.*dtBin./Tau2)./Tau2;
        
        convfBG=Convol(shiftIRF,f)+c;
        
        fGG=convfBG(ROI(1):ROI(2))./sum(convfBG(ROI(1):ROI(2)));
        
        Mi=NGG.*fGG;
        
        gof=2/(length(Ni)-length(params))*sum(Ni.*log(Ni./Mi)); % For Maximum Likelihood Estimation method
        %       gof=sum(((Ni-Mi).^2)./Ni)/(length(Ni)-length(params)-1); % Alternative: reduced Chi2
        
    end

    function [gof,Mi,shiftIRF]=MLE_New(params)
        
        ke=1./params(1);
        pe=params(2);
        c=params(3);
        shift=params(4);
        Fnorm=(1-pe)*pr1+(1-pe)*(1-pr1)+pr1*pe*kr1/(kr1+ke)+pe*(1-pr1)*kr2/(kr2+ke);
        Fr1=(1-pe)*pr1/Fnorm; %Donors in state 1 that cannot undergo FRET
        Fr2=(1-pe)*(1-pr1)/Fnorm; %Donors in state 2 that cannot undergo FRET
        Fe1=pr1*pe*kr1/(kr1+ke)/Fnorm; %Donors in state 1 that can undergo FRET
        Fe2=pe*(1-pr1)*kr2/(kr2+ke)/Fnorm; %Donors in state 2 that can undergo FRET
        
        
        if shift>=0
            shiftIRF=[zeros(round(shift),1);normIRF(1:end-round(shift))];
        else
            shiftIRF=[normIRF(1+abs(round(shift)):end);zeros(abs(round(shift)),1)];
        end
        
        f=Fr1*exp(-kr1*xfine.*dtBin)*kr1+Fe1*exp(-(kr1+ke)*xfine.*dtBin)*(kr1+ke)...
            +Fr2*exp(-(kr2)*xfine.*dtBin)*(kr2)+Fe2*exp(-(kr2+ke)*xfine.*dtBin)*(kr2+ke);
        
        convfBG=Convol(shiftIRF,f)+c;
        
        fGG=convfBG(ROI(1):ROI(2))./sum(convfBG(ROI(1):ROI(2)));
        
        Mi=NGG.*fGG;
        
        gof=2/(length(Ni)-length(params))*sum(Ni.*log(Ni./Mi)); % For Maximum Likelihood Estimation method
        %       gof=sum(((Ni-Mi).^2)./Ni)/(length(Ni)-length(params)-1); % Alternative: reduced Chi2
        
    end
end
