% This script is used to calculate average fluorescence lifetimes from sets
% of decay curves (without fitting).
%
% INPUT: decay curves as CSV files (first column: times, second column:
% number of counts per pixel). The CSV files must be distributed in
% subfolders named according to the respective experimental condition.
% OUTPUT: boxplot of the average lifetime distributions as a
% MATLAB FIG file and calculated average lifetime values in a MATLAB MAT
% file and in a TXT file.
function Average_tau()
path=''; %folder containing the CSV files, distributed into subfolders according to their respective experimental condition
list=dir(path); %list of the subfolders
DirecLabels=list([list.isdir]);
DirecLabels=DirecLabels(3:end);
close all

%Four conditions:
Pattern1='sensing'; %string pattern to be found in name of subfolder with force-sensing construct with or without drug data
Pattern2='ROCK';  %string pattern to be found in name of subfolder with force-sensing construct (or zero-force construct) with drug data
Pattern3='Zero'; %string pattern to be found in name of subfolder with zero-force construct data
Pattern4='only'; %string pattern to be found in name of subfolder with mCerulean only transfection data

DrugOnFS=1; %Set to 1 if the data in the 'ROCK' subfolders is from cells with force-sensing construct and drug present. Set to 0 if they are from cells with the zero-force construct and drug present.

%% main script
addpath('scripts');

histD_total={[]};
taus=nan(4,30); %average fluorescence lifetime, one per condition

% importing data
for DirecN=1:length(DirecLabels)
    Files=dir([path char(DirecLabels(DirecN).name) '/Cell*.csv']);
    for iterC=1:length(Files)
        
        csvFile=importdata([path DirecLabels(DirecN).name '/' Files(iterC).name]);
        histD_total{iterC}=csvFile.data(:,2);
        
        if iterC==1
            
            edgesT=csvFile.data(:,1)./1000; % (ns)
        end
    end
    
    % defining channels for fitting
    dtBin=(edgesT(2)-edgesT(1)); % width of one bin in the decay histogram (ns)
  
      
    % Calculating the average lifetime
    for iterF=1:length(Files)
        [~,imax]=max(histD_total{iterF});%t=0 is set at the emission peak
        temp=0;
        for i=1:length(histD_total{iterF})%Includes negative times: photons that arrived before the peak
            temp=temp+dtBin*(i-imax)*histD_total{iterF}(i);
        end
        temp=temp/sum(histD_total{iterF}(1:end));%Normalisation
        
        
        % Save results in text file:
        file_id = fopen([path 'Output_Average_tau_withNegTimes.txt'],'a');
        fprintf(file_id,'%s \t',Files(iterF).name);
        fprintf(file_id,'%g \t',temp);
        fprintf(file_id,'\n');
        fclose(file_id);
        
        if DrugOnFS %Case where the drug was added to cells expressing the force-sensing construct
            if (length(strfind(char(DirecLabels(DirecN).name),Pattern1))+length(strfind(char(DirecLabels(DirecN).name),Pattern2)))>1
            %condition 2: force-sensing with inhibitor
            index=min(find(isnan(taus(2,:))));
            taus(2,index)=temp;
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern1))==1
            %condition 1: force-sensing without inhibitor
            index=min(find(isnan(taus(1,:))));
            taus(1,index)=temp;
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern3))==1 & length(strfind(char(DirecLabels(DirecN).name),Pattern2))==0
            %condition 3: zero-force construct
            index=min(find(isnan(taus(3,:))));
            taus(3,index)=temp;
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern4))>0
            %condition 4: only cerulean
            index=min(find(isnan(taus(4,:))));
            taus(4,index)=temp;
            end
            
        else %Case where the drug was added to cells expressing the zero-force construct
            if (length(strfind(char(DirecLabels(DirecN).name),Pattern3))+length(strfind(char(DirecLabels(DirecN).name),Pattern2)))>1
            %condition 2prime: zero-force with inhibitor
            index=min(find(isnan(taus(2,:))));
            taus(2,index)=temp;
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern1))==1
            %condition 1: force-sensing without inhibitor
            index=min(find(isnan(taus(1,:))));
            taus(1,index)=temp;
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern3))==1 & length(strfind(char(DirecLabels(DirecN).name),Pattern2))==0
            %condition 3: zero-force construct
            index=min(find(isnan(taus(3,:))));
            taus(3,index)=temp;
            elseif length(strfind(char(DirecLabels(DirecN).name),Pattern4))>0
            %condition 4: only cerulean
            index=min(find(isnan(taus(4,:))));
            taus(4,index)=temp;
            end
        end
    end   
end
    figure;
    boxplot(taus');
    title('Average lifetime from the time of peak emission (ns)')
    if DrugOnFS
        xticklabels({'force-sensing','force-sens.+inhi.','zero-force','only Cerulean'})
    else
        xticklabels({'--','zero-force + inhib.','zero-force','--'})
    end
    saveas(gcf, [path 'Average_lifetimes.fig']);
    save([path 'Average_lifetimes.mat'], 'taus'); %Saving the average lifetimes in a Matlab MAT file
1